<?php
include_once('CaordaMarquee_ShortCodeLoader.php');

class CaordaMarquee_ShortCode_cmitem extends CaordaMarquee_ShortCodeLoader {
    /**
     * @param  $atts shortcode inputs
     * @return string shortcode content
     */
    public function handleShortcode($atts, $content) {

        $out_content = isset($atts['url']) ? '<a href="'.$atts['url'].'" target="'.$atts['target'].'">'
            .do_shortcode($content).'</a>' : do_shortcode($content);

        $output = '<div class="mchild mchildstyle">';
        $output .= $out_content;
        $output .= '</div>';

        return $output;
    }
}