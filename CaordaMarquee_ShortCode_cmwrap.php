<?php
include_once('CaordaMarquee_ShortCodeScriptLoader.php');

class CaordaMarquee_ShortCode_cmwrap extends CaordaMarquee_ShortCodeScriptLoader {

    static $addedAlready = false;
    public function handleShortcode($atts, $content) {

        $style = "";
        $start_color = $atts['start_color'];
        $end_color = $atts['end_color'];

        if($start_color || $end_color){
            if(!$end_color) $end_color = $start_color;
            $style = "
background: $start_color;
background: -moz-linear-gradient(top,  $start_color 0%, $end_color 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,$start_color), color-stop(100%,$end_color));
background: -webkit-linear-gradient(top,  $start_color 0%,$end_color 100%);
background: -o-linear-gradient(top,  $start_color 0%,$end_color 100%);
background: -ms-linear-gradient(top,  $start_color 0%,$end_color 100%);
background: linear-gradient(to bottom,  $start_color 0%,$end_color 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='$start_color', endColorstr='$end_color',GradientType=0 );
            ";
        }

        $output = '<div class="marquee" style="'.$style.'">';
        $output .= do_shortcode( $content );
        $output .= '</div>';
        return $output;
    }

    public function addScript() {
        if (!self::$addedAlready) {
            self::$addedAlready = true;
            wp_register_script('fmarquee', plugins_url('assets/fmarquee/fmarquee/jquery.fmarquee.min.js', __FILE__), array('jquery'), '1.0', true);
            wp_register_script('fmarquee_start', plugins_url('js/fmarquee_start.js', __FILE__), array('jquery'), '1.0', true);
            wp_print_scripts('fmarquee');
            wp_print_scripts('fmarquee_start');
        }
    }
}