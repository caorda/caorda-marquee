jQuery(document).ready(function($){


        //$('.cm-color-field').wpColorPicker();

	/*==========================================
	=            Add marquee inputs            =
	==========================================*/
	$(document).on('click', '.cm-input .dashicons-plus', function(){

		var $cont = $(this).parent();
		$cont.clone().insertAfter( $cont );
	});


	/*=============================================
	=            Remove marquee inputs            =
	=============================================*/
	$(document).on('click', '.cm-input .dashicons-minus', function(){

		var $cont = $(this).parent();
		$cont.fadeOut(500, function(){
			$cont.remove();
		});
	});

});