<?php


include_once('CaordaMarquee_LifeCycle.php');
include_once('CaordaMarquee_Widget_cmwidget.php');

class CaordaMarquee_Plugin extends CaordaMarquee_LifeCycle {

    /**
     * See: http://plugin.michael-simpson.com/?page_id=31
     * @return array of option meta data.
     */
    public function getOptionMetaData() {
        //  http://plugin.michael-simpson.com/?page_id=31
        return array(
            //'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
            //'ATextInput' => array(__('Enter in some text', 'my-awesome-plugin')),
            //'Donated' => array(__('I have donated to this plugin', 'my-awesome-plugin'), 'false', 'true'),
            //'CanSeeSubmitData' => array(__('Can See Submission data', 'my-awesome-plugin'),
            //                            'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber', 'Anyone')
        );
    }

//    protected function getOptionValueI18nString($optionValue) {
//        $i18nValue = parent::getOptionValueI18nString($optionValue);
//        return $i18nValue;
//    }

    protected function initOptions() {
        $options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }
    }

    public function getPluginDisplayName() {
        return 'Caorda Marquee';
    }

    protected function getMainPluginFileName() {
        return 'caorda-marquee.php';
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
        //            `id` INTEGER NOT NULL");
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
    }


    /**
     * Perform actions when upgrading from version X to version Y
     * See: http://plugin.michael-simpson.com/?page_id=35
     * @return void
     */
    public function upgrade() {
    }

    public function addActionsAndFilters() {

        // Add options administration page
        // http://plugin.michael-simpson.com/?page_id=47
        add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));

        // Example adding a script & style just for the options administration page
        // http://plugin.michael-simpson.com/?page_id=47


        // Add Actions & Filters
        // http://plugin.michael-simpson.com/?page_id=37
        add_action( 'admin_enqueue_scripts', array(&$this, 'cm_admin_enqueue' ));

        // Adding scripts & styles to all pages
        // Examples:
        //        wp_enqueue_script('jquery');
        //        wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
        wp_enqueue_style('caorda_fmarquee', plugins_url('/css/caorda_fmarquee.css', __FILE__));




        // Register short codes
        // http://plugin.michael-simpson.com/?page_id=39
        include_once('CaordaMarquee_ShortCode_cmwrap.php');
            $sc = new CaordaMarquee_ShortCode_cmwrap();
            $sc->register('cmwrap');

        include_once('CaordaMarquee_ShortCode_cmitem.php');
            $sc = new CaordaMarquee_ShortCode_cmitem();
            $sc->register('cmitem');


        // Register AJAX hooks
        // http://plugin.michael-simpson.com/?page_id=41

    }

    public function cm_admin_enqueue( $hook ){
        if( 'widgets.php' != $hook )
            return;

        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script('caorda_fmarquee_admin', plugins_url('/js/caorda_fmarquee_admin.js', __FILE__));
        wp_enqueue_style('caorda_fmarquee_admin', plugins_url('/css/caorda_fmarquee_admin.css', __FILE__));

    } // cm_admin_enqueue


}
