<?php

// Creating the widget
class cm_widget extends WP_Widget {

	function __construct() {

		parent::__construct(
			// Base ID of your widget
			'cm_widget',

			// Widget name will appear in UI
			__('Caorda Marquee', 'cm_widget_domain'),

			// Widget description
			array( 'description' => __( 'Adds a marquee', 'cm_widget_domain' ), )
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {

		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

		// This is where you run the code and display the output
		$shortcode = '[cmwrap start_color="'.$instance['start_color'].'" end_color="'.$instance['end_color'].'"]';
		foreach($instance['text'] as $k=>$text){
			$target = $instance['target'][$k];
			$url = $instance['url'][$k];

			$shortcode .= '[cmitem target="'.$target.'" url="'.$url.'"]';
			$shortcode .= do_shortcode($text );
			$shortcode .= '[/cmitem]';
		}
		$shortcode .= '[/cmwrap]';


		echo do_shortcode($shortcode);
		echo $args['after_widget'];

	}

	// Widget Backend
	public function form( $instance ) {

		if( empty($instance['url']) ) $instance['url'] = array("");
		if( empty($instance['text']) ) $instance['text'] = array("");
		if( empty($instance['target']) ) $instance['target'] = array("");
		$title = isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : __( 'Marquee', 'cm_widget_domain' );
		$start_color = isset( $instance[ 'start_color' ] ) ? $instance[ 'start_color' ] : "";
		$end_color = isset( $instance[ 'end_color' ] ) ? $instance[ 'end_color' ] : "";
		$targets = array('_self'=>'Self', '_blank'=>'New', '_parent'=>'Parent', '_top'=>'Top');

		// Widget admin form
		?>
		<div class="cm-widget">
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'start_color' ); ?>"><?php _e( 'Start Color:' ); ?></label>
			<input class="widefat cm-color-field" id="<?php echo $this->get_field_id( 'start_color' ); ?>" name="<?php echo $this->get_field_name( 'start_color' ); ?>" type="text" value="<?php echo esc_attr( $start_color ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'end_color' ); ?>"><?php _e( 'End Color:' ); ?></label>
			<input class="widefat cm-color-field" id="<?php echo $this->get_field_id( 'end_color' ); ?>" name="<?php echo $this->get_field_name( 'end_color' ); ?>" type="text" value="<?php echo esc_attr( $end_color ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Text:' ); ?></label><br />
			<div class="cm-inputs">

			<?php
			foreach($instance['text'] as $k=>$text):
				$url = $instance['url'][$k];
				$target = $instance['target'][$k];
			?>

				<div class="cm-input">
					<input placeholder="link URL" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>[]" type="text" value="<?php echo htmlentities($url); ?>" />
					<input placeholder="text" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>[]" type="text" value="<?php echo htmlentities($text); ?>" />
					<select id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'target' ); ?>[]">
						<?php foreach($targets as $tk=>$t):
							$t_sel = ($target == $tk) ? 'selected="selected"' : "";
						?>
							<option value="<?php echo $tk; ?>" <?php echo $t_sel; ?>><?php echo $t; ?></option>
						<?php endforeach; ?>
					</select>

					<span class="dashicon dashicons-before dashicons-plus"><br></span>
					<span class="dashicon dashicons-before dashicons-minus"><br></span>
				</div>

			<?php endforeach; ?>

			</div>
		</p>
		</div>

		<script type="text/javascript">
			jQuery(document).ready(function($){
	        	$('.cm-color-field').wpColorPicker();
	        });
		</script>
		<?php
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ?  $new_instance['text'] : '';
		$instance['url'] = ( ! empty( $new_instance['url'] ) ) ?  $new_instance['url'] : '';
		$instance['target'] = ( ! empty( $new_instance['target'] ) ) ?  $new_instance['target'] : '';
		$instance['start_color'] = ( ! empty( $new_instance['start_color'] ) ) ?  $new_instance['start_color'] : '';
		$instance['end_color'] = ( ! empty( $new_instance['end_color'] ) ) ?  $new_instance['end_color'] : '';
		return $instance;
	}

} // Class cm_widget ends here

// Register and load the widget
function cm_load_widget() {
	register_widget( 'cm_widget' );
}
add_action( 'widgets_init', 'cm_load_widget' );