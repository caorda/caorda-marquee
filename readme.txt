=== Caorda Marquee ===
Contributors: Caorda Web Solutions
Donate link:
Tags:
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Requires at least: 3.5
Tested up to: 3.5
Stable tag: 0.1

Provides a marquee banner widget and shortcode

== Description ==

Provides a marquee banner widget and shortcode

Use as follows:

[cmwrap][cmitem]Item 1[/cmitem][cmitem]Item 2[/cmitem][/cmwrap]

== Installation ==


== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1 =
- Initial Revision
